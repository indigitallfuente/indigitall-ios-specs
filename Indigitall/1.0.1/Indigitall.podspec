Pod::Spec.new do |s|
    s.name              = 'Indigitall'
    s.version           = '1.0.1'
    s.summary           = 'A really cool SDK that logs stuff.'
    s.homepage          = 'http://example.com/'

    s.author            = { 'Name' => 'sdk@example.com' }
    s.license           = { :type => 'Apache-2.0', :file => 'LICENSE' }

    s.platform          = :ios
    s.source            = { :git => 'https://bitbucket.org/indigitallfuente/indigitall-ios-pods', :tag => s.version.to_s }

    s.ios.deployment_target = '9.0'
    s.ios.vendored_frameworks = 'Indigitall.framework'
end
